package com.example.residenciapp

import androidx.appcompat.app.AppCompatActivity
import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_areas.*
import kotlinx.android.synthetic.main.activity_areas.inicio

class activity_areas : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_areas)

        apartar1.setOnClickListener {

            val intent = Intent(this, activity_calendario::class.java)
            startActivity(intent)
        }
        apartar2.setOnClickListener {

            val intent = Intent(this, activity_calendario::class.java)
            startActivity(intent)
        }
        inicio.setOnClickListener {
            val intent = Intent(this, activity_menu::class.java)
            startActivity(intent)

        }
    }
}