package com.example.residenciapp

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        TvGoToRegister.setOnClickListener {
            Toast.makeText(this, "Registro", Toast.LENGTH_SHORT).show()

            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        Entrar.setOnClickListener {
            val intent = Intent(this, activity_menu::class.java)
            startActivity(intent)

        }

        /*val= variable que no puede cambiar, es una constante
        * intent clase de java para hacer un evento se necesitan los otros dos parametros
        * staractivity recibe un intent al presionarlo
        * */
    }

}
