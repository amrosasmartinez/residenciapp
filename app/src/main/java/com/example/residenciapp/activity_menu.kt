package com.example.residenciapp

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_menu.*

class activity_menu : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        Area_comun.setOnClickListener {

            val intent = Intent(this, activity_areas::class.java)
            startActivity(intent)
        }

        avisos.setOnClickListener {

            val intent = Intent(this, activity_avisos::class.java)
            startActivity(intent)
        }

        mantenimiento.setOnClickListener {

            val intent = Intent(this, mantenimiento::class.java)
            startActivity(intent)
        }


        sugerencias.setOnClickListener {

            val intent = Intent(this, activity_quejas::class.java)
            startActivity(intent)
        }

        emergencia.setOnClickListener {

            val intent = Intent(this, activity_emergencia::class.java)
            startActivity(intent)
        }

        visitas.setOnClickListener {

            val intent = Intent(this, activity_visitas::class.java)
            startActivity(intent)
        }
    }
}
