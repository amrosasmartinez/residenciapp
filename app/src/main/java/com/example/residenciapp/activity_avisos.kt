package com.example.residenciapp


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_avisos.inicio

class activity_avisos : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_avisos)

        inicio.setOnClickListener {
            val intent = Intent(this, activity_menu::class.java)
            startActivity(intent)

        }

    }
}