package com.example.residenciapp


import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_emergencia.*


class activity_emergencia : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emergencia)

        inicio.setOnClickListener {
            val intent = Intent(this, activity_menu::class.java)
            startActivity(intent)

        }

    }
}